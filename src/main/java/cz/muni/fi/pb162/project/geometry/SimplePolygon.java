package cz.muni.fi.pb162.project.geometry;


import cz.muni.fi.pb162.project.exception.MissingVerticesException;

import java.util.List;

import static cz.muni.fi.pb162.project.utils.SimpleMath.minX;
import static cz.muni.fi.pb162.project.utils.SimpleMath.maxX;
import static cz.muni.fi.pb162.project.utils.SimpleMath.minY;
import static cz.muni.fi.pb162.project.utils.SimpleMath.maxY;

/**
 * @author Patrik Brosz (485694)
 */

public abstract class SimplePolygon implements Polygon{

    /**
     * Overloaded constructor of class SimplePolygon used to check array of vertices
     * @param listOfVertices array of objects Vertex2D
     * @throws MissingVerticesException throws exception if there is not enough vertices in list
     */
    public SimplePolygon(Vertex2D[] listOfVertices) throws MissingVerticesException {
        if(listOfVertices == null){
            throw new IllegalArgumentException("Array is not valid");
        }

        if(listOfVertices.length < 3){
            throw new MissingVerticesException("Not enough vertices");
        }

        for (Vertex2D vertex : listOfVertices) {
            if (vertex == null) {
                throw new IllegalArgumentException("Array is not valid");
            }
        }
    }

    /**
     * Overloaded constructor of class SimplePolygon used to check list of vertices
     * @param listOfVertices list of objects Vertex2D
     * @throws MissingVerticesException throws exception if there is not enough vertices in list
     */
    public SimplePolygon(List<Vertex2D> listOfVertices) throws MissingVerticesException {
        if(listOfVertices == null){
            throw new IllegalArgumentException("List is not valid");
        }

        if(listOfVertices.size() < 3){
            throw new MissingVerticesException("Not enough vertices");
        }

        for (Vertex2D vertex: listOfVertices) {
            if(vertex == null){
                throw new IllegalArgumentException("List is not valid");
            }
        }
    }

    public double getWidth(){
        return maxX(this) - minX(this);
    }

    public double getHeight(){
        return maxY(this) - minY(this);
    }

    @Override
    public String toString() {

        StringBuilder string = new StringBuilder("Polygon: vertices =");
        for(int i = 0; i < this.getNumVertices(); i++){
            string.append(" "+ getVertex(i));
        }
        return string.toString();
    }
}
