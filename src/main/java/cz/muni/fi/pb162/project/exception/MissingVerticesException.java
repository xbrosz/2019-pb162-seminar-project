package cz.muni.fi.pb162.project.exception;

/**
 * @author Patrik Brosz
 */
public class MissingVerticesException extends RuntimeException{
    /**
     * Overloaded constructor of exception class MissingVerticesException
     * @param s string which will be printed on screen
     */
    public MissingVerticesException(String s){
        super(s);
    }

    /**
     * Overloaded constructor of exception class MissingVerticesException
     * @param s string which will be printed on screen
     * @param cause cause of exception
     */
    public MissingVerticesException(String s, Throwable cause){
        super(s, cause);
    }
}
