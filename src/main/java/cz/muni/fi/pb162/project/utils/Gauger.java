package cz.muni.fi.pb162.project.utils;
import cz.muni.fi.pb162.project.geometry.Measurable;
import cz.muni.fi.pb162.project.geometry.Triangle;

/**
 * @author Patrik Brosz (485694)
 */

public class Gauger {
    /**
     * Overloaded method used to print width and height of the object
     * @param measurable object of type Measurable, obtains height and width
     */
    public static void printMeasurement(Measurable measurable) {
        System.out.println("Width: " + measurable.getWidth());
        System.out.println("Height: " + measurable.getHeight());
    }

    /**
     * Overloaded method used to print vertices, width and height of the triangle
     * @param triangle object of type Triangle, obtains height and width
     */
    public static void printMeasurement(Triangle triangle) {
        System.out.println(triangle.toString());
        printMeasurement((Measurable)triangle);
    }
}
