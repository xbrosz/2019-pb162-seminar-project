package cz.muni.fi.pb162.project.geometry;

import java.util.Objects;

/**
 * @author Patrik Brosz (485694)
 */
public class ColoredPolygon{
    private Polygon polygon;
    private Color color;

    /**
     * Constructor used to initialize polygon and color of the polygon
     * @param polygon polygon of type Polygon
     * @param color color of the polygon
     */
    public ColoredPolygon(Polygon polygon, Color color) {
        this.polygon = polygon;
        this.color = color;
    }

    /**
     * Getter used to get Polygon
     * @return polygon of type Polygon
     */
    public Polygon getPolygon() {
        return polygon;
    }

    /**
     * Getter used to get color of the Polygon
     * @return color
     */
    public Color getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ColoredPolygon that = (ColoredPolygon) o;

        return Objects.equals(polygon, that.polygon) &&
                color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(polygon, color);
    }
}
