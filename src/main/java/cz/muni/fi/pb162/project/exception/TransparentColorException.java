package cz.muni.fi.pb162.project.exception;

/**
 * @author Patrik Brosz
 */
public class TransparentColorException extends Exception {
    /**
     * Overloaded constructor of exception class TransparentColorException
     * @param s string which will be printed on screen
     */
    public TransparentColorException(String s){
        super(s);
    }

    /**
     * Overloaded constructor of exception class TransparentColorException
     * @param s string which will be printed on screen
     * @param cause cause of exception
     */
    public TransparentColorException(String s, Throwable cause){
        super(s, cause);
    }
}
