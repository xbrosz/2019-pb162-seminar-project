package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */

public class GeneralRegularPolygon implements RegularPolygon,Colored {
    private Vertex2D center;
    private int numOfEdges;
    private double radius;
    private Color color;

    /**
     * Constructor used to initialize center,numOfEdges, radius and color to black
     * @param center center of polygon
     * @param numOfEdges number of polygon edges
     * @param radius radius of polygon
     */
    public GeneralRegularPolygon(Vertex2D center, int numOfEdges, double radius) {
        this.center = center;
        this.numOfEdges = numOfEdges;
        this.radius = radius;
        setColor(Color.BLACK);
    }

    /**
     * Getter used to get length of edge
     * @return length of edge
     */
    public double getEdgeLength() {
        return 2*getRadius()*java.lang.Math.sin(Math.PI/getNumEdges());
    }

    /**
     * Getter used to get width of polygon
     * @return width of polygon
     */
    public double getWidth() {
        return getRadius() * 2;
    }

    /**
     * Getter used to get height of polygon
     * @return height of polygon
     */
    public double getHeight() {
        return getRadius() * 2;
    }

    /**
     * Setter used to set color of polygon
     * @param color color of type Color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Getter used to get color of polygon
     * @return polygon color of type Color
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * Getter used to get num of polygon edges
     * @return num of polygon edges
     */
    public int getNumEdges() {
        return this.numOfEdges;
    }

    @Override
    public String toString() {
        return this.numOfEdges + "-gon: center=" + this.center.toString() + ", radius=" + this.radius + ", color="
                + this.getColor().toString();
    }

    /**
     * Getter used to get vertex of polygon on index i
     * @param i index of vertex, used to calculate vertex
     * @return vertex on index i
     */
    public Vertex2D getVertex(int i) {
        return new Vertex2D((center.getX() - getRadius() * java.lang.Math.cos(i * 2 * Math.PI / getNumEdges())),
                            (center.getY() - getRadius() * java.lang.Math.sin(i * 2 * Math.PI / getNumEdges())));
    }

    /**
     * Getter used to get center of polygon
     * @return polygon center of type Vertex2D
     */
    public Vertex2D getCenter() {
        return this.center;
    }

    /**
     * Getter used to get radius of polygon
     * @return radius of polygon
     */
    public double getRadius() {
        return this.radius;
    }
}
