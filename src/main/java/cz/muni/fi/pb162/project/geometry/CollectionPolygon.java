package cz.muni.fi.pb162.project.geometry;

import cz.muni.fi.pb162.project.exception.MissingVerticesException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static cz.muni.fi.pb162.project.utils.SimpleMath.minX;

/**
 * @author Patrik Brosz (485694)
 */
public class CollectionPolygon extends SimplePolygon {
    private List<Vertex2D> vertices;

    /**
     * Overloaded constructor of class CollectionPolygon used to initialize vertices
     * @param listOfVertices array of vertices of type Vertex2D
     * @throws MissingVerticesException throws exception if there is not enough vertices in list
     */
    public CollectionPolygon(Vertex2D[] listOfVertices)  throws MissingVerticesException{
        super(listOfVertices);

        vertices = new ArrayList<>(Arrays.asList(listOfVertices));
    }

    /**
     * Overloaded constructor of class CollectionPolygon used to initialize vertices
     * @param listOfVertices list of vertices of type Vertex2D
     * @throws MissingVerticesException throws exception if there is not enough vertices in list
     */
    public CollectionPolygon(List<Vertex2D> listOfVertices) throws MissingVerticesException {
        super(listOfVertices);

        vertices = new ArrayList<>(listOfVertices);
    }

    /**
     * Method used to return list of vertices without left most vertices
     * @return object of type CollectionPolygon without left most vertices
     * @throws MissingVerticesException throws exception if there is not enough vertices in list
     */
    public CollectionPolygon withoutLeftmostVertices() throws MissingVerticesException {
        double filter = minX(this);
        List<Vertex2D> newList = new ArrayList<>();

        if(vertices == null || vertices.size() <= 0){
            throw new IllegalArgumentException("empty");
        }

        for (Vertex2D vertex: vertices) {
            if(Double.compare(filter, vertex.getX()) != 0){
                newList.add(vertex);
            }
        }
        return new CollectionPolygon(newList);
    }

    @Override
    public Vertex2D getVertex(int i) {
        if(i < 0){
            throw new IllegalArgumentException("Entered index is negative");
        }
        return vertices.get(i % this.getNumVertices());
    }

    @Override
    public int getNumVertices() {
        return vertices.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CollectionPolygon that = (CollectionPolygon) o;

        return Objects.equals(vertices, that.vertices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vertices);
    }

}
