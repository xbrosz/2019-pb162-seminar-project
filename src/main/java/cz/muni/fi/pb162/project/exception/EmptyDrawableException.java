package cz.muni.fi.pb162.project.exception;

/**
 * @author Patrik Brosz
 */
public class EmptyDrawableException extends Exception {
    /**
     * Overloaded constructor of exception class EmptyDrawableException
     * @param s string which will be printed on screen
     */
    public EmptyDrawableException(String s){
        super(s);
    }

    /**
     * Overloaded constructor of exception class EmptyDrawableException
     * @param s string which will be printed on screen
     * @param cause cause of exception
     */
    public EmptyDrawableException(String s, Throwable cause){
        super(s, cause);
    }
}
