package cz.muni.fi.pb162.project.geometry;

import static cz.muni.fi.pb162.project.utils.SimpleMath.maxX;
import static cz.muni.fi.pb162.project.utils.SimpleMath.maxY;
import static cz.muni.fi.pb162.project.utils.SimpleMath.minX;
import static cz.muni.fi.pb162.project.utils.SimpleMath.minY;

/**
 *
 * @author Patrik Brosz (485649)
 */

public class Triangle extends ArrayPolygon implements Measurable{
    private static final double ERROR = 0.001;

    /**
     * Array containing 3 points of the triangle
     */


    /**
     * Array containing divided triangle into 3 new triangles
     */
    private final Triangle[] triangles = new Triangle[3];

    /**
     * Constructor for class Triangle
     * @param a Vertex2D point of the triangle
     * @param b Vertex2D point of the triangle
     * @param c Vertex2D point of the triangle
     */
    public Triangle(Vertex2D a, Vertex2D b, Vertex2D c) {
        super(new Vertex2D[]{a,b,c});

    }

    /**
     * Constructor for class Triangle
     * @param a Vertex2D point of the triangle
     * @param b Vertex2D point of the triangle
     * @param c Vertex2D point of the triangle
     * @param depth number of dividing triangle
     */
    public Triangle(Vertex2D a, Vertex2D b, Vertex2D c, int depth) {
        super(new Vertex2D[]{a,b,c});
        divide(depth);
    }


    /**
     * Method used for printing vertices of the triangle
     * @return horizontal and vertical distances of 3 points
     */
    @Override
    public String toString() {
        return "Triangle: vertices=" + getVertex(0).toString() + " " +
                getVertex(1).toString() + " " + getVertex(2).toString();
    }

    /**
     * This method checks whether triangle was divided
     * @return true if triangle was divided, otherwise false
     */
    public boolean isDivided() {
        return this.triangles[0] != null || this.triangles[1] != null || this.triangles[2] != null;
    }

    /**
     * Method divides one triangle into 3 new triangles
     * @return true if triangle wasn't already divided, otherwise false
     */
    public boolean divide() {
        if (!isDivided()){
            triangles[0] = new Triangle(new Vertex2D(getVertex(0).getX(),getVertex(0).getY()),
                    getVertex(0).createMiddle(getVertex(1)),
                    getVertex(0).createMiddle(getVertex(2)));



            triangles[1] = new Triangle(getVertex(0).createMiddle(getVertex(1)),
                    new Vertex2D(getVertex(1).getX(), getVertex(1).getY()),
                    getVertex(1).createMiddle(getVertex(2)));


            triangles[2] = new Triangle(new Vertex2D(getVertex(2).getX(), getVertex(2).getY()),
                    getVertex(0).createMiddle(getVertex(2)),
                    getVertex(1).createMiddle(getVertex(2)));

            return true;
        }
        return false;
    }

    /**
     * Method used to get one of the subtriangles
     * @param  index index of one of the subtriangles
     * @return subtriangle if index is in range of array, otherwise null
     */
    public Triangle getSubTriangle(int index) {
        if (index >=0 && index <= 2){
            return triangles[index];
        }
        return null;
    }

    /**
     * Method used to check if triangle is equilateral
     * @return true, if triangle is equilateral, otherwise false
     */
    public boolean isEquilateral() {
        return Math.abs(getVertex(0).distance(getVertex(1)) - getVertex(0).distance(getVertex(2))) < ERROR &&
                Math.abs(getVertex(0).distance(getVertex(1)) - getVertex(1).distance(getVertex(2))) < ERROR;
    }

    /**
     * Method used to get one of the subtriangles
     * @param  depth number of dividing triangles
     */
    public void divide(int depth) {
        if (depth > 0){
            divide();
            triangles[0].divide(depth-1);
            triangles[1].divide(depth-1);
            triangles[2].divide(depth-1);
        }
    }

    @Override
    public double getWidth(){
        return maxX(this) - minX(this);
    }

    @Override
    public double getHeight(){
        return maxY(this) - minY(this);
    }
}