package cz.muni.fi.pb162.project.geometry;

import java.util.Objects;

/**
 *
 * @author Patrik Brosz (485694)
 */

public class Vertex2D {
    /**
     * The horizontal and vertical distances of point (x,y)
     */
    private final double x;
    private final double y;

    /**
     * Constructor for class Triangle
     * @param x the horizontal distance of point
     * @param y the vertical distance of point
     */
    public Vertex2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Constructor for class Triangle
     * @param vertex other point of type Vertex2D
     * @return distance of two points
     */
    public double distance(Vertex2D vertex) {
        if (vertex == null){
            return -1.0;
        }
        return Math.sqrt((vertex.getX() - this.getX()) * (vertex.getX() -
                this.getX()) + (vertex.getY() - this.getY()) * (vertex.getY() - this.getY()));
    }

    @Override
    public String toString(){
        return "[" + this.getX() + ", " + this.getY()+ "]";
    }

    /**
     * Creats new point between 2 points
     * @param otherVertex another vertex used to calculate middle point
     * @return new middle point
     */
    public Vertex2D createMiddle(Vertex2D otherVertex) {
        Vertex2D newVertex = new Vertex2D((this.x + otherVertex.x)/2, (this.y + otherVertex.y)/2);
        return newVertex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Vertex2D vertex2D = (Vertex2D) o;
        return Double.compare(vertex2D.x, x) == 0 &&
                Double.compare(vertex2D.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}