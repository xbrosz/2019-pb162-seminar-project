package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */

public class Circle extends GeneralRegularPolygon implements Measurable,Circular {

    /**
     * Constructor of class Circle used to initialize color, center and radius of the circle
     * @param center center of the circle of type Vertex2D
     * @param radius radius of the circle of type double
     */
    public Circle(Vertex2D center, double radius) {
        super(center, Integer.MAX_VALUE, radius);
        this.setColor(Color.RED);
    }

    /**
     * Constructor of class Circle used to initialize center to [0,0] and radius to 1
     */
    public Circle() {
        this(new Vertex2D(0.0, 0.0), 1); }

    @Override
    public String toString() {
        return "Circle: center=[" + getCenter().getX() + ", " + getCenter().getY() + "], radius=" + getRadius();
    }

    @Override
    public double getEdgeLength() {
        return 0;
    }

}