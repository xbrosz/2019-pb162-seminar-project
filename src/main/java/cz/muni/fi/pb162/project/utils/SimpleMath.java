package cz.muni.fi.pb162.project.utils;
import cz.muni.fi.pb162.project.geometry.Polygon;

/**
 * @author Patrik Brosz (485694)
 */

public class SimpleMath {

    /**
     * Method used to print minimum X (horizontal distance) of the polygon
     * @param polygon object of type Triangle
     * @return minimum X (vertical distance)
     */
    public static double minX(Polygon polygon) {
        double min = Double.POSITIVE_INFINITY;
        for(int i = 0; i < polygon.getNumVertices(); i++){
            if(polygon.getVertex(i).getX() < min){
                min = polygon.getVertex(i).getX();
            }
        }
        return min;
    }

    /**
     * Method used to print minimum Y (vertical distance) of the polygon
     * @param polygon object of type Triangle
     * @return minimum Y (vertical distance)
     */
    public static double minY(Polygon polygon) {
        double min = Double.POSITIVE_INFINITY;
        for(int i = 0; i < polygon.getNumVertices(); i++){
            if(polygon.getVertex(i).getY() < min){
                min = polygon.getVertex(i).getY();
            }
        }
        return min;
    }

    /**
     * Method used to print maximum X (horizontal distance) of the polygon
     * @param polygon object of type Triangle
     * @return maximum X (vertical distance)
     */
    public static double maxX(Polygon polygon) {
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < polygon.getNumVertices(); i++) {
            if(polygon.getVertex(i).getX() > max){
                max = polygon.getVertex(i).getX();
            }
        }
        return max;
    }

    /**
     * Method used to print maximum Y (vertical distance) of the polygon
     * @param polygon object of type Triangle
     * @return maximum Y (vertical distance)
     */
    public static double maxY(Polygon polygon) {
        double max = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < polygon.getNumVertices(); i++) {
            if (polygon.getVertex(i).getY() > max) {
                max = polygon.getVertex(i).getY();
            }
        }
        return max;
    }
}
