package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */

public enum Color {
    BLUE, RED, GREEN, BLACK, YELLOW, WHITE, ORANGE;

    @Override
    public String toString() {
        switch(this){
            case BLUE: return "blue";
            case RED: return "red";
            case GREEN: return "green";
            case BLACK: return "black";
            case YELLOW: return "yellow";
            case WHITE: return "white";
            case ORANGE: return "orange";
            default: return "ERROR";
        }
    }
}
