package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */

public class Square extends GeneralRegularPolygon implements Circular{

    /**
     * Overloaded constructor used to set center and diameter of the circle(square), calls super class's constructor
     * @param center center if the square of type Vertex2D
     * @param diameter diameter of the circle
     */
    public Square(Vertex2D center, double diameter){
        super(center, 4, diameter/2);
    }

    /**
     * Overloaded constructor used to set center and diameter of the circle(square), it calls another constructor
     * @param circular is square or circle of type Circular
     */
    public Square(Circular circular){
        this(circular.getCenter(), (circular.getRadius()*2));
    }


    @Override
    public String toString() {
        return "Square: vertices=" +
                            "[" + this.getVertex(0).getX() + ", " +this.getVertex(0).getY() + "] "+
                            "[" + this.getVertex(1).getX() + ", " +this.getVertex(1).getY() + "] "+
                            "[" + this.getVertex(2).getX() + ", " +this.getVertex(2).getY() + "] "+
                            "[" + this.getVertex(3).getX() + ", " +this.getVertex(3).getY() + "]";
    }
}
