package cz.muni.fi.pb162.project.geometry;

import cz.muni.fi.pb162.project.exception.EmptyDrawableException;
import cz.muni.fi.pb162.project.exception.MissingVerticesException;
import cz.muni.fi.pb162.project.exception.TransparentColorException;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

/**
 * @author Patrik Brosz
 */
public class Paper implements Drawable, PolygonFactory {
    private Set<ColoredPolygon> coloredPolygons;
    private Color color;

    /**
     * Overloaded constructor of class Paper used to initialize coloredPolygons and drawing color to black
     */
    public Paper(){
        coloredPolygons = new HashSet<>();
        color = Color.BLACK;
    }

    /**
     * Overloaded constructor of class Paper used to initialize coloredPolygons
     * @param drawable all polygons
     */
    public Paper(Drawable drawable){
        coloredPolygons = new HashSet<>(drawable.getAllDrawnPolygons());
    }

    @Override
    public void changeColor(Color color) {
        this.color = color;
    }

    @Override
    public void drawPolygon(Polygon polygon) throws TransparentColorException{
        if(color == Color.WHITE){
            throw new TransparentColorException("Cannot draw with white");
        }

        coloredPolygons.add(new ColoredPolygon(polygon,color));
    }

    @Override
    public void erasePolygon(ColoredPolygon polygon) {
        coloredPolygons.remove(polygon);
    }

    @Override
    public void eraseAll() throws EmptyDrawableException {
        if(coloredPolygons.size() == 0){
            throw new EmptyDrawableException("Paper is already empty");
        }

        coloredPolygons.clear();
    }

    @Override
    public Collection<ColoredPolygon> getAllDrawnPolygons() {
        return Collections.unmodifiableCollection(coloredPolygons);
    }

    @Override
    public int uniqueVerticesAmount() {
        Set<Vertex2D> vertices = new HashSet<>();

        for (ColoredPolygon polygon:coloredPolygons) {
            for (int i = 0; i < polygon.getPolygon().getNumVertices(); i++) {
                vertices.add(polygon.getPolygon().getVertex(i));
            }
        }

        return vertices.size();
    }

    /**
     * Method use do create polygon
     * @param vertices collection which the polygon can be built from; the collection is not modified
     * @return Polygon created using vertices
     * @throws MissingVerticesException
     */
    @Override
    public Polygon tryToCreatePolygon(List<Vertex2D> vertices) throws MissingVerticesException {
        if(vertices == null){
            throw new NullPointerException("list is null");
        }

        Polygon polygon;
        List<Vertex2D> copyVertices = new ArrayList<>(vertices);

        try {
            polygon = new CollectionPolygon(copyVertices);
        } catch (IllegalArgumentException e) {
            copyVertices.removeIf(Objects::isNull);
            polygon = new CollectionPolygon(copyVertices);
        }

        return polygon;
    }

    /**
     * Method used to draw polygons
     * @param collectionPolygons collection of polygons (every polygon is collection of vertices)
     * @throws EmptyDrawableException
     */
    @Override
    public void tryToDrawPolygons(List<List<Vertex2D>> collectionPolygons) throws EmptyDrawableException {
        Iterator<List<Vertex2D>> iterator = collectionPolygons.iterator();
        Throwable x = new Throwable();
        Polygon polygon;

        while(iterator.hasNext()) {
            try {
                polygon = tryToCreatePolygon(iterator.next());
                try {
                    drawPolygon(polygon);
                } catch (TransparentColorException e) {
                    changeColor(Color.BLACK);
                    x = e;
                }

            } catch (NullPointerException | MissingVerticesException e) {
                x = e;
            }
        }
        if(coloredPolygons.isEmpty()) {
            throw new EmptyDrawableException(x.getMessage(),x);
        }

        }

    /**
     * Method used to return collection of polygons with entered color
     * @param color color of polygon
     * @return collection of polygons
     */
    public Collection<Polygon> getPolygonsWithColor(Color color){
        Collection<Polygon> polygonsWithColor = new HashSet<>();
        List<Vertex2D> vertices= new ArrayList<>();

        for (ColoredPolygon polygon:  coloredPolygons) {
            if(polygon.getColor().equals(color)){
                for(int i = 0; i < polygon.getPolygon().getNumVertices(); i++){
                    vertices.add(polygon.getPolygon().getVertex(i));
                }
                polygonsWithColor.add(new CollectionPolygon(vertices));
                vertices.clear();
            }
        }
        return polygonsWithColor;
    }
}
