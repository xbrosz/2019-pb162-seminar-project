package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */
public interface Colored {

    /**
     * Getter used to get color of type Color
     * @return color of object
     */
    Color getColor();

    /**
     * Setter used to set object color
     * @param color object color of type Color
     */
    void setColor(Color color);
}
