package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */

public class RegularOctagon extends GeneralRegularPolygon {

    /**
     * Constructor of class RegularOctagon which calls constructor of super class and initialize object variables
     * @param center center of octagon
     * @param radius radius of octagon
     */
    public RegularOctagon(Vertex2D center, double radius){
        super(center, 8, radius);
    }
}
