package cz.muni.fi.pb162.project.geometry;

/**
 * @author Patrik Brosz (485694)
 */

public class Snowman {
    private static final double MIN_FACTOR = 0.8;

    /**
     * Array of 4 Circulars, 4 parts of the snowman
     */
    private RegularPolygon[] polygons = new GeneralRegularPolygon[3];

    /**
     * Getter used to get all balls of snowman
     * @return array of 4 circulars
     */
    public RegularPolygon[] getBalls(){
        return polygons;
    }

    /**
     * Method used to make new ball
     * @param polygon object located under new object
     * @param minimizeFactor number used to calculate dimensions of new object
     * @return new ball of snowman
     */
    private GeneralRegularPolygon buildBall(RegularPolygon polygon, double minimizeFactor) {
        return new GeneralRegularPolygon(new Vertex2D(polygon.getCenter().getX(),
                                        (polygon.getCenter().getY() + polygon.getRadius() +
                                                polygon.getRadius() * minimizeFactor)),
                                        polygon.getNumEdges(), (polygon.getRadius()*minimizeFactor));
    }

    /**
     * Constructor used to initialize lowermost ball and minimizeFactor
     * @param regularPolygon lowermost object of type RegularPolygon
     * @param minimizeFactor minimization factor used to calculate dimension of new object
     */
    public Snowman(RegularPolygon regularPolygon, double minimizeFactor) {
        if (!(minimizeFactor > 0 && minimizeFactor < 1)) {
            minimizeFactor = MIN_FACTOR;
        }

        polygons[0] = regularPolygon;
        for (int i = 1; i < 3; i++){
            polygons[i] =  buildBall(polygons[i-1], minimizeFactor);
        }
    }
}
