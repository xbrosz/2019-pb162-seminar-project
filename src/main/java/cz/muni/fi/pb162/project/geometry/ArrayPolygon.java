package cz.muni.fi.pb162.project.geometry;

import cz.muni.fi.pb162.project.exception.MissingVerticesException;
import java.util.Arrays;

/**
 * @author Patrik Brosz (485694)
 */
public class ArrayPolygon extends SimplePolygon{
    private Vertex2D[] listOfVertices;

    /**
     * Constructor of class ArrayPolygon used to set vertices
     * @param listOfVertices vertices of type Vertex2D
     * @throws MissingVerticesException throws exception if there is not enough vertices in list
     */
    public ArrayPolygon(Vertex2D[] listOfVertices) throws MissingVerticesException {
        super(listOfVertices);
        this.listOfVertices = Arrays.copyOf(listOfVertices, listOfVertices.length);
    }

    @Override
    public Vertex2D getVertex(int i) {
        if(i < 0){
            throw new IllegalArgumentException("Entered index is negative");
        }

        return listOfVertices[i % this.getNumVertices()];
    }

    @Override
    public int getNumVertices() {
        return listOfVertices.length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ArrayPolygon that = (ArrayPolygon) o;

        return Arrays.equals(listOfVertices, that.listOfVertices);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(listOfVertices);
    }
}
